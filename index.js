// khai báo thư viện express 
const express = require("express");
//-----b1khai báo thư viện mongoose-----
var mongoose = require('mongoose');
const { diceHistoryRouter } = require("./app/routes/diceHistoryRouter");
const { ramdomNumberRouter } = require("./randomnumberAPIRouter");
const { userRouter } = require("./app/routes/userRouter");

// khai báo app 
const app = express();

// Cấu hình request đọc được body json
app.use(express.json());

//khai báo port
const port = 8000;

app.listen(port, () => {
    console.log(`App listen on port  ${port}`)
})
const path = require("path");
const { prizeRouter } = require("./app/routes/prizeRouter");
const { voucherRouter } = require("./app/routes/voucherRouter");
const { prizeHistoryRouter } = require("./app/routes/prizeHistoryRouter");
const { voucherHistoryRouter } = require("./app/routes/voucherHIstoryRouter");

//  app.get('/', (request,respone) => {
//     respone.sendFile(path.join(__dirname + "/views/23B.40.html"))
//  })
 app.use('/',ramdomNumberRouter  );
//thêm ảnh vào middleware static
app.use(express.static(__dirname + '/views'))

//app chạy trên UserRouters
app.use('/',userRouter);

//app chạy trên diceHistoryRouters
app.use('/',diceHistoryRouter);

//app chạy trên PrizeRouter
app.use('/',prizeRouter);
//app chạy trên voucherRouter
app.use('/',voucherRouter);

//app chạy trên prizeHistory
app.use('/',prizeHistoryRouter);

//app chạy trên voucherHistory
app.use('/',voucherHistoryRouter);

// kết nối với mongoosebd
main().catch(err => console.log(err));

async function main() {
  await mongoose.connect('mongodb://127.0.0.1:27017/CRUD_DICE');
  console.log('Successfully connected mongoDB');
  
}