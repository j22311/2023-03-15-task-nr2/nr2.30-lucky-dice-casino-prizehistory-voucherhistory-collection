// Khai báo thư viên mongooseJS
const mongoose = require("mongoose");

// Khai báo Class Schema
const Schema = mongoose.Schema;

// Khởi tạo 1 instance courseSchema từ class Schema
const voucherSchema = new Schema({
    code: {
        type: String,
        required: true,
        unique: true
       
    },
    discount: {
        type: Number,
        required: true,
       
    },
    note: {
        type: String,
        required: false,
       
    },
    // Một course có nhiều review
    // cars: [{
    //     type: mongoose.Types.ObjectId,
    //     ref: "Cars"
    // }]
    createdAt: {
        type: Date,
        default: Date.now(),
    },
    updatedAt: {
        type: Date,
        default: Date.now(),
    },
    
}, {
    timestamps: true
})

module.exports = mongoose.model("Vouchers", voucherSchema);