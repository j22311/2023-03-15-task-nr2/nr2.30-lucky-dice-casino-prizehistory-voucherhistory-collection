// Import thư viện mongoose
const mongoose = require("mongoose");

// Import diceHistorys Model
const diceHistoryModel = require('../models/diceHistoryModel');

const getAlldiceHistory = (request,response) => {
    // b1: thu thập dữ liệu
    // b2: validate dữ liệu 
    // b3: gọi model tạo dữ liệu 
    
    diceHistoryModel.find()
   .then((data) => {
      response.status(201).json({
         message: 'Successful to show all diceHistory',
         diceHistory: data
      })
   })
   .catch((error)=> {
      response.status(500).json({
         message: `internal sever ${error}`
      })
    })
   
 }

 const getAlldiceHistoryByUserId = (request,response) => {
   // b1: thu thập dữ liệu
   // b2: validate dữ liệu 
   // b3: gọi model tạo dữ liệu 
   const userId = request.params.userId;
   const condition = {};
   if(userId){
      condition.user = {$eq: userId};
   }
   diceHistoryModel.find(condition)
  .then((data) => {
     response.status(201).json({
        message: 'Successful to show all diceHistory by UserId',
        diceHistory: data
     })
  })
  .catch((error)=> {
     response.status(500).json({
        message: `internal sever ${error}`
     })
   })
  
}

 //lấy một dữ liệu nước uống bằng id
//-------------------------get a drink by ID 
const getAndiceHistoryById = (request,response) => {
    //b1: thu thập dữ liệu
    const diceHistoryId = request.params.diceHistoryId;
 
    //b2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(diceHistoryId)){
       return response.status(400).json({
          status: 'Bad Request',
          message: `${diceHistoryId} ko hợp lệ`
       })
    }
    //bb3: gọi model tìm dữ liêu
    diceHistoryModel.findById(diceHistoryId)
       .then((data) => {
          response.status(201).json({
             message: 'successful to find one ',
             productType: data
          })
       })
       .catch((error) => {
          response.status(500).json({
             message: `internal sever ${error}`
          })
       })
 }
 
const creatediceHistory = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const body = request.body;
   
    
    // B2: Validate dữ liệu
    // Kiểm tra title có hợp lệ hay không
    if(!mongoose.Types.ObjectId.isValid(body.user)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "user không hợp lệ"
        })
    }

    if(isNaN(body.dice) || body.dice < 0) {
        return response.status(400).json({
            status: "Bad Request",
            message: "dice  không hợp lệ"
        })
    }


   

    // B3: Gọi Model tạo dữ liệu
    const newdiceHistory = {
       
        user: body.user,
        dice: body.dice,
        
    }

    diceHistoryModel.create(newdiceHistory)
     .then((data) => {
          response.status(201).json({
             message: 'Successful to create new newdiceHistory',
             dice: data
          })
     })
     .catch((error)=> {
       response.status(500).json({
          message: `internal sever ${error}`
       })
     })
}

const updatediceHistoryById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const diceHistoryId = request.params.diceHistoryId;
    const body = request.body;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "CourseID không hợp lệ"
        })
    }

   
    if(!mongoose.Types.ObjectId.isValid(body.user)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "user không hợp lệ"
        })
    }

    if(isNaN(body.dice) || body.dice < 0) {
        return response.status(400).json({
            status: "Bad Request",
            message: "dice  không hợp lệ"
        })
    }
    const vCurrentTime = new Date();
    // B3: Gọi Model tạo dữ liệu
    const updatediceHistory = {
        user: body.user,
        dice: body.dice,
        
         updatedAt: vCurrentTime
    }

    

    diceHistoryModel.findByIdAndUpdate(diceHistoryId,updatediceHistory)
       .then((data) => {
          response.status(201).json({
             message: 'Successful to update new diceHistory',
             diceHistory: data
          })
       })
       .catch((error) => {
          response.status(500).json({
          message: `internal sever ${error}`
          })
       })
}

const deletediceHistoryByID = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const diceHistoryId = request.params.diceHistoryId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(diceHistoryId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "diceHistoryId không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    diceHistoryModel.findByIdAndDelete(diceHistoryId)
    .then((data) => {
       response.status(201).json({
          message: 'Successful to delete an diceHistory ',
          productType: data
       })
    })
    .catch((error) => {
       response.status(500).json({
       message: `internal sever ${error}`
       })
    })
}

module.exports = {
    deletediceHistoryByID,
    updatediceHistoryById,
    creatediceHistory,
    getAndiceHistoryById,
    getAlldiceHistory,
    getAlldiceHistoryByUserId
}