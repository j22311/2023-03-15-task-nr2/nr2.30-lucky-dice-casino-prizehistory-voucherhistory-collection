// Import thư viện mongoose
const mongoose = require("mongoose");

// Import voucherHistorys Model
const voucherHistoryModel = require('../models/voucherHistoryModel');

const getAllVoucherHistory = (request,response) => {
    // b1: thu thập dữ liệu
    // b2: validate dữ liệu 
    // b3: gọi model tạo dữ liệu 
    voucherHistoryModel.find()
   .then((data) => {
      response.status(201).json({
         message: 'Successful to show all voucherHistory',
         voucherHistory: data
      })
   })
   .catch((error)=> {
      response.status(500).json({
         message: `internal sever ${error}`
      })
    })
   
 }
 const getAllVoucherHistoryByUserId = (request,response) => {
   // b1: thu thập dữ liệu
   // b2: validate dữ liệu 
   // b3: gọi model tạo dữ liệu 
   const userId = request.params.userId;
   const condition = {};
   if(userId){
      condition.user = {$eq: userId};
   }
   diceHistoryModel.find(condition)
  .then((data) => {
     response.status(201).json({
        message: 'Successful to show all diceHistory by UserId',
        diceHistory: data
     })
  })
  .catch((error)=> {
     response.status(500).json({
        message: `internal sever ${error}`
     })
   })
  
}
 //lấy một dữ liệu nước uống bằng id
//-------------------------get a drink by ID 
const getAVoucherHistoryById = (request,response) => {
    //b1: thu thập dữ liệu
    const voucherHistoryId = request.params.voucherHistoryId;
 
    //b2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherHistoryId)){
       return response.status(400).json({
          status: 'Bad Request',
          message: `${voucherHistoryId} ko hợp lệ`
       })
    }
    //bb3: gọi model tìm dữ liêu
    voucherHistoryModel.findById(voucherHistoryId)
       .then((data) => {
          response.status(201).json({
             message: 'successful to find one ',
             voucherHistory: data
          })
       })
       .catch((error) => {
          response.status(500).json({
             message: `internal sever ${error}`
          })
       })
 }
 
const createVoucherHistory = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const body = request.body;
   
    
    // B2: Validate dữ liệu
    // Kiểm tra title có hợp lệ hay không
    console.log(body);
    if(!mongoose.Types.ObjectId.isValid(body.user)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "user không hợp lệ"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(body.voucher)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "voucher không hợp lệ"
        })
    }

    

   console.log(body);

    // B3: Gọi Model tạo dữ liệu
    const newvoucherHistory = {
       
        user: body.user,
        voucher: body.voucher,
        
    }

    voucherHistoryModel.create(newvoucherHistory)
     .then((data) => {
          response.status(201).json({
             message: 'Successful to create new newvoucherHistory',
             prize: data
          })
     })
     .catch((error)=> {
       response.status(500).json({
          message: `internal sever ${error}`
       })
     })
}

const updateVoucherHistoryById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const voucherHistoryId = request.params.voucherHistoryId;
    const body = request.body;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherHistoryId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "voucherHistoryId không hợp lệ"
        })
    }

   
    if(!mongoose.Types.ObjectId.isValid(body.user)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "user không hợp lệ"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(body.voucher)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "user không hợp lệ"
        })
    }
    const vCurrentTime = new Date();
    // B3: Gọi Model tạo dữ liệu
    const updatevoucherHistory = {
        user: body.user,
        prize: body.prize,
        
        updatedAt: vCurrentTime
    }

    

    voucherHistoryModel.findByIdAndUpdate(voucherHistoryId,updatevoucherHistory)
       .then((data) => {
          response.status(201).json({
             message: 'Successful to update new voucherHistory',
             voucherHistory: data
          })
       })
       .catch((error) => {
          response.status(500).json({
          message: `internal sever ${error}`
          })
       })
}

const deleteVoucherHistoryByID = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const voucherHistoryId = request.params.voucherHistoryId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherHistoryId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "voucherHistoryId không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    voucherHistoryModel.findByIdAndDelete(voucherHistoryId)
    .then((data) => {
       response.status(201).json({
          message: 'Successful to delete an voucherHistory ',
          productType: data
       })
    })
    .catch((error) => {
       response.status(500).json({
       message: `internal sever ${error}`
       })
    })
}

module.exports = {
    deleteVoucherHistoryByID,
    updateVoucherHistoryById,
    createVoucherHistory,
    getAVoucherHistoryById,
    getAllVoucherHistory,
    getAllVoucherHistoryByUserId
}