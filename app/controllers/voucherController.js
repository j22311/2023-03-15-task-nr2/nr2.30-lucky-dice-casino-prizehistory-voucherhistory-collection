// Import thư viện mongoose
const mongoose = require("mongoose");

// Import vouchers Model
const voucherModel = require('../models/voucherModel');

const getAllVoucher = (request,response) => {
    // b1: thu thập dữ liệu
    // b2: validate dữ liệu 
    // b3: gọi model tạo dữ liệu 
    voucherModel.find()
   .then((data) => {
      response.status(201).json({
         message: 'Successful to show all voucher',
         voucher: data
      })
   })
   .catch((error)=> {
      response.status(500).json({
         message: `internal sever ${error}`
      })
    })
   
 }

 //lấy một dữ liệu nước uống bằng id
//-------------------------get a drink by ID 
const getAnVoucherById = (request,response) => {
    //b1: thu thập dữ liệu
    const voucherId = request.params.voucherId;
 
    //b2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherId)){
       return response.status(400).json({
          status: 'Bad Request',
          message: `${voucherId} ko hợp lệ`
       })
    }
    //bb3: gọi model tìm dữ liêu
    voucherModel.findById(voucherId)
       .then((data) => {
          response.status(201).json({
             message: 'successful to find one ',
             productType: data
          })
       })
       .catch((error) => {
          response.status(500).json({
             message: `internal sever ${error}`
          })
       })
 }
 
const createVoucher = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const body = request.body;
   
    
    // B2: Validate dữ liệu
    // Kiểm tra title có hợp lệ hay không
    if( !body.code ) {
        return response.status(400).json({
            status: "Bad Request",
            message: "name  không hợp lệ"
        })
    }

    if( !body.discount ) {
        return response.status(400).json({
            status: "Bad Request",
            message: "discount  không hợp lệ"
        })
    }



   

    // B3: Gọi Model tạo dữ liệu
    const newVoucher = {
       
        code: body.code,
        discount: body.discount,
        
    }

    voucherModel.create(newVoucher)
     .then((data) => {
          response.status(201).json({
             message: 'Successful to create new voucher',
             voucher: data
          })
     })
     .catch((error)=> {
       response.status(500).json({
          message: `internal sever ${error}`
       })
     })
}

const updateVoucherById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const voucherId = request.params.voucherId;
    const body = request.body;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "CourseID không hợp lệ"
        })
    }

   
 
    if(body.code !== undefined && body.code.trim() === "") {
        return response.status(400).json({
            status: "Bad Request",
            message: "code không hợp lệ"
        })
    }

    if(isNaN(body.discount) || body.discount <= 0) {
        console.log(body.discount);
        return response.status(400).json({
            status: "Bad Request",
            message: "discount không hợp lệ"
        })
    }
    const vCurrentTime = new Date();
    // B3: Gọi Model tạo dữ liệu
    const updateVoucher = {
        code: body.code,
        discount: body.discount,
        
         updatedAt: vCurrentTime
    }

    

    voucherModel.findByIdAndUpdate(voucherId,updateVoucher)
       .then((data) => {
          response.status(201).json({
             message: 'Successful to update new voucher',
             voucher: data
          })
       })
       .catch((error) => {
          response.status(500).json({
          message: `internal sever ${error}`
          })
       })
}

const deleteVoucherByID = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const voucherId = request.params.voucherId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "voucherId không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    voucherModel.findByIdAndDelete(voucherId)
    .then((data) => {
       response.status(201).json({
          message: 'Successful to delete an voucher ',
          productType: data
       })
    })
    .catch((error) => {
       response.status(500).json({
       message: `internal sever ${error}`
       })
    })
}

module.exports = {
    deleteVoucherByID,
    updateVoucherById,
    createVoucher,
    getAnVoucherById,
    getAllVoucher
}