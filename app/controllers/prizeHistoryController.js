// Import thư viện mongoose
const mongoose = require("mongoose");

// Import prizeHistorys Model
const prizeHistoryModel = require('../models/prizeHistoryModel');

const getAllPrizeHistory = (request,response) => {
    // b1: thu thập dữ liệu
    // b2: validate dữ liệu 
    // b3: gọi model tạo dữ liệu 
    prizeHistoryModel.find()
   .then((data) => {
      response.status(201).json({
         message: 'Successful to show all prizeHistory',
         prizeHistory: data
      })
   })
   .catch((error)=> {
      response.status(500).json({
         message: `internal sever ${error}`
      })
    })
   
 }
 const getAllPrizeHistoryByUserId = (request,response) => {
    // b1: thu thập dữ liệu
    // b2: validate dữ liệu 
    // b3: gọi model tạo dữ liệu 
    const userId = request.params.userId;
    const condition = {};
    if(userId){
       condition.user = {$eq: userId};
    }
    diceHistoryModel.find(condition)
   .then((data) => {
      response.status(201).json({
         message: 'Successful to show all diceHistory by UserId',
         diceHistory: data
      })
   })
   .catch((error)=> {
      response.status(500).json({
         message: `internal sever ${error}`
      })
    })
   
 }
 
 //lấy một dữ liệu nước uống bằng id
//-------------------------get a drink by ID 
const getAPrizeHistoryById = (request,response) => {
    //b1: thu thập dữ liệu
    const prizeHistoryId = request.params.prizeHistoryId;
 
    //b2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(prizeHistoryId)){
       return response.status(400).json({
          status: 'Bad Request',
          message: `${prizeHistoryId} ko hợp lệ`
       })
    }
    //bb3: gọi model tìm dữ liêu
    prizeHistoryModel.findById(prizeHistoryId)
       .then((data) => {
          response.status(201).json({
             message: 'successful to find one ',
             prizeHistory: data
          })
       })
       .catch((error) => {
          response.status(500).json({
             message: `internal sever ${error}`
          })
       })
 }
 
const createPrizeHistory = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const body = request.body;
   
    
    // B2: Validate dữ liệu
    // Kiểm tra title có hợp lệ hay không
    if(!mongoose.Types.ObjectId.isValid(body.user)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "user không hợp lệ"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(body.prize)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "user không hợp lệ"
        })
    }

    

   console.log(body);

    // B3: Gọi Model tạo dữ liệu
    const newprizeHistory = {
       
        user: body.user,
        prize: body.prize,
        
    }

    prizeHistoryModel.create(newprizeHistory)
     .then((data) => {
          response.status(201).json({
             message: 'Successful to create new newprizeHistory',
             prize: data
          })
     })
     .catch((error)=> {
       response.status(500).json({
          message: `internal sever ${error}`
       })
     })
}

const updatePrizeHistoryById = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const prizeHistoryId = request.params.prizeHistoryId;
    const body = request.body;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(prizeHistoryId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "CourseID không hợp lệ"
        })
    }

   
    if(!mongoose.Types.ObjectId.isValid(body.user)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "user không hợp lệ"
        })
    }
    if(!mongoose.Types.ObjectId.isValid(body.prize)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "user không hợp lệ"
        })
    }
    const vCurrentTime = new Date();
    // B3: Gọi Model tạo dữ liệu
    const updateprizeHistory = {
        user: body.user,
        prize: body.prize,
        
        updatedAt: vCurrentTime
    }

    

    prizeHistoryModel.findByIdAndUpdate(prizeHistoryId,updateprizeHistory)
       .then((data) => {
          response.status(201).json({
             message: 'Successful to update new prizeHistory',
             prizeHistory: data
          })
       })
       .catch((error) => {
          response.status(500).json({
          message: `internal sever ${error}`
          })
       })
}

const deletePrizeHistoryByID = (request, response) => {
    // B1: Chuẩn bị dữ liệu
    const prizeHistoryId = request.params.prizeHistoryId;

    // B2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(prizeHistoryId)) {
        return response.status(400).json({
            status: "Bad Request",
            message: "prizeHistoryId không hợp lệ"
        })
    }

    // B3: Gọi Model tạo dữ liệu
    prizeHistoryModel.findByIdAndDelete(prizeHistoryId)
    .then((data) => {
       response.status(201).json({
          message: 'Successful to delete an prizeHistory ',
          productType: data
       })
    })
    .catch((error) => {
       response.status(500).json({
       message: `internal sever ${error}`
       })
    })
}

module.exports = {
    deletePrizeHistoryByID,
    updatePrizeHistoryById,
    createPrizeHistory,
    getAPrizeHistoryById,
    getAllPrizeHistory,
    getAllPrizeHistoryByUserId
}